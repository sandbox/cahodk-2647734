/**
 * @file
 * Parses and initializes the theaterJS from the field value.
 */

var theaterJS = theaterJS || {};

(function ($, Drupal, theaterJS) {

  'use strict';

  Drupal.behaviors.largeTable = {
    attach: function (context, settings) {
      $('.theater-hidden', context).once('initTheater').each(initTheater);

      function initTheater() {
        var that = this;
        // Flatten and validate field content.
        var str = $(this).text().replace(/\n/g, " ");
        var go = str.match(/^([A-Za-z0-9]+?\s*:\s*((".*?")|(,\s*\d+))*\s*;\s*)*$/);

        if (go) {
          var actors = [];
          var scenes = [];

          // Explode the content and build up scene from atoms.
          var scenesArray = str.split(";").filter(function (s) {
            return s.trim().length > 0;
          });

          scenesArray.forEach(function (scene) {
            scene = scene.split(':');

            var actor = scene[0].trim().toLowerCase();
            if (!actor.match(/^[a-z0-9\-_]+$/)) {
              return false;
            }

            if (actors.indexOf(actor) === -1) {
              actors.push(actor);
            }

            var atoms = [];
            scene[1].split(',').forEach(function (atom) {
              atom = atom.trim();
              var isString = atom.match(/^"(.*)"$/);
              var isNumber = atom.match(/^([0-9]+)$/);
              if (isString) {
                atoms.push(isString[1]);
              }
              if (isNumber) {
                atoms.push(parseInt(isNumber[1]));
              }
            });
            scenes.push({actor: actor, atoms: atoms});
          });

          var theater = theaterJS();

          theater
            .on('type:start, erase:start', function () {
              // Add a class to actor's dom element when he starts typing/erasing.
              var actor = theater.getCurrentActor();
              actor.$element.classList.add('is-typing');
            })
            .on('type:end, erase:end', function () {
              // And then remove it when he's done.
              var actor = theater.getCurrentActor();
              actor.$element.classList.remove('is-typing');
            });

          actors.forEach(function (actor) {
            $(that).parent().append('<div id="' + actor + '" class="theater-actor"/>');
            theater.addActor(actor);
          });

          scenes.forEach(function (scene) {
            var sceneArray = [];
            sceneArray.push(scene.actor + ":" + scene.atoms[0]);
            for (var i = 1; i < scene.atoms.length; i++) {
              sceneArray.push(scene.atoms[i]);
            }
            theater.addScene.apply(this, sceneArray);
          });
        }
      }
    }
  };

})(jQuery, Drupal, theaterJS);
