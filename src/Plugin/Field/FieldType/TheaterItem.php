<?php
/**
 * @file
 * Contains \Drupal\theater\Plugin\Field\FieldType\TheaterItem.
 */

namespace Drupal\theater\Plugin\Field\FieldType;

use Drupal\Component\Utility\Xss;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Plugin implementation of the 'theater' field type.
 *
 * @FieldType(
 *   id = "theater",
 *   label = @Translation("Theater"),
 *   module = "theater",
 *   description = @Translation("This field is used to display a text in a typewriter style."),
 *   default_widget = "theater_textarea",
 *   default_formatter = "theater_default"
 * )
 */
class TheaterItem extends TextLongItem {

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()
      ->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    // Validate each actor and scene atom
    // against regular expressions and Xss::filter.
    $callback = function ($value, ExecutionContextInterface $context) {
      $this->theaterValidate($value, $context);
    };

    $options = [
      'value' => [
        'Callback' => [
          'callback' => $callback,
        ],
      ],
    ];

    $constraints[] = $constraint_manager->create('ComplexData', $options);

    return $constraints;
  }

  /**
   * Field validation.
   *
   * Custom validation that validates the actors and atomic elements of a scene,
   * against a number of regular expressions and XSS filtering.
   * First the input is split into a multi-array by semi-colon, colon and
   * commas.
   *
   * Scenes: Semi-colon separated sentences, just like PHP instructions.
   *
   * Actor: First part of a colon separated array. Represents who is talking.
   *
   * Atom: Second part of a colon separated array (Actor/sentence) is a comma
   * separated array alternating strings and numbers - what is said, and the
   * pause between them.
   *
   * @param string $value
   *   Field input.
   * @param \Symfony\Component\Validator\Context\ExecutionContextInterface $context
   *   The Symfony execution context.
   *
   * @return bool
   *    Return true if there are no validation errors.
   */
  public function theaterValidate($value, ExecutionContextInterface $context) {
    if (empty($value)) {
      return $this->addViolation($context, t("Input is empty"));
    }

    // Split the text into an array by colon and semi-colon.
    if (!$txt = $this->splitInput($value, $needle)) {
      return $this->addViolation($context,
        t("No @needle found.", ['@needle' => $needle]));
    }

    for ($x = 0; $x < count($txt); $x++) {
      // Scenes: Semi-colon separated sentences, just like PHP instructions.
      $scene = $txt[$x];

      if (!$this->validateActor($scene, $actor)) {
        return $this->addViolation($context,
          t("Actor is invalid: @actor", ['@actor' => $actor]));
      }

      // Validate each atom in the scene.
      for ($i = 0; $i < count($scene[1]); $i++) {
        $atom = trim($scene[1][$i]);
        // Validate if the atom string contains scripting.
        if ($this->atomIsScript($atom)) {
          return $this->addViolation($context,
            t('The field contains scripting: @atom', ['@atom' => $atom]));
        }

        // Validate the wait time as digit.
        if ($i % 2 !== 0 && !$this->isDigit($atom)) {
          return $this->addViolation($context,
            t('The wait time should be a digit: @atom', ['@atom' => $atom]));
        }
      }
    }
  }

  /**
   * @param $context
   * @param $arg
   */
  protected function addViolation($context, $arg) {
    $context
      ->buildViolation($arg)
      ->addViolation();
    return FALSE;
  }

  /**
   * Splits an input on semi-colon, colon and comma.
   *
   * @param string $value
   *   The field input.
   *
   * @return array
   *   The input split into an array for later processing.
   */
  protected function splitInput($value, &$needle) {
    // Split by semi-colon to get each scene.
    $needle = ';';
    $scenes = preg_split("/(?<!\\\);/", $value);
    // No splitting occurred.
    if (count($scenes) === 1) {
      return FALSE;
    }

    // Split by colon to get the actor and the sentence.
    $needle = ':';
    $sentences = [];
    foreach ($scenes as $scene) {
      if (empty($scene)) {
        continue;
      }
      $sentence = explode($needle, $scene, 2);
      if (count($sentence) === 1) {
        return FALSE;
      }
      $sentences[] = $sentence;
    }

    $needle = ',';
    $atoms = [];
    foreach ($sentences as $sentence) {
      $sentence_atoms = preg_split("/(?<!\\\),/", $sentence[1]);
      $atoms[] = [$sentence[0], $sentence_atoms];
    }
    return $atoms;
  }

  /**
   * Validate if the actor is alphanumeric.
   *
   * @param array $scene
   *   The input array contains the actor and the sentence.
   * @param string $actor
   *   Assign the actor to this string argument.
   *
   * @return bool
   *   TRUE if the actor matches a string of alphanumeric letters and numbers.
   *   The actor is returned as a reference in the input argument $actor.
   */
  protected function validateActor(array $scene, &$actor) {
    $actor = trim($scene[0]);
    if (!preg_match("/^[a-z0-9]+$/", $actor)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Does string contains scripting, by comparing the input to a filtered copy.
   *
   * @param string $atom
   *   The input string to validate for scripting.
   *
   * @return bool
   *   Return true if string contains scripting.
   */
  protected function atomIsScript($atom) {
    $filtered = Xss::filter($atom);
    if ($filtered !== $atom) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Validate if the input is a true digit.
   *
   * @param string $atom
   *   The string to test.
   *
   * @return bool
   *   Return true if the input is a digit.
   */
  protected function isDigit($atom) {
    if (!preg_match("/^\d+?$/", $atom)) {
      return FALSE;
    }
    return TRUE;
  }

}
