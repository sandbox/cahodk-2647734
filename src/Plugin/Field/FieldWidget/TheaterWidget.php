<?php
/**
 * @file
 * Contains \Drupal\theater\Plugin\Field\FieldWidget\TheaterWidget.
 */

namespace Drupal\theater\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'theater_textarea' widget.
 *
 * @FieldWidget(
 *   id = "theater_textarea",
 *   label = @Translation("Theater script"),
 *   field_types = {
 *     "theater"
 *   }
 * )
 */
class TheaterWidget extends StringTextareaWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $main_widget = parent::formElement($items, $delta, $element, $form, $form_state);

    $description = '<p>' . t('Example input:') . '</p>';
    $description .= '<pre>' . t('vader:"Luke...", 400;') . '</pre>';
    $description .= '<pre>' . t('luke:"What?", 400;') . '</pre>';
    $description .= '<pre>' . t('vader:"I am", 200, ".", 200, ".", 200, ". ", 200, "Your father!";') . '</pre>';

    $element = $main_widget['value'];
    $element['#type'] = 'text_format';
    $element['#format'] = $items[$delta]->format;
    $element['#base_type'] = $main_widget['value']['#type'];
    $element['#description'] = $description;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    if ($violation->arrayPropertyPath == ['format'] && isset($element['format']['#access']) && !$element['format']['#access']) {
      // Ignore validation errors for formats if formats may not be changed,
      // i.e. when existing formats become invalid. See filter_process_format().
      return FALSE;
    }
    return $element;
  }

}
