<?php

/**
 * @file
 * Contains \Drupal\theater\Plugin\Field\FieldFormatter\TheaterFormatter.
 */

namespace Drupal\theater\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'theater_default' formatter.
 *
 * @FieldFormatter(
 *   id = "theater_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "theater",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class TheaterFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [''],
      ],
      '#attached' => [
        'library' => [
          'theater/theater.theaterjs',
          'theater/theater.theater_init',
        ],
      ],
    ];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => '<div class="theater-hidden">' . $item->value . '</div>',
        '#attributes' => [
          'class' => [
            'theater-item',
          ],
          'id' => 'theater-item-' . $delta,
        ],
        '#attached' => [
          'drupalSettings' => [
            'theater' => [
              $delta => [
                'scene' => $item,
              ],
            ],
          ],
        ],
      ];
    }

    return $elements;
  }

}
