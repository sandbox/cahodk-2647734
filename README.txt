CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Maintainers


 INTRODUCTION
 ------------

 The Theater module lets you easily create a simplified version of TheaterJS.
 It defines a field, that lets you insert some simple markup, that outputs
 as a TheaterJS scene.


 REQUIREMENTS
 ------------

 This module requires the TheaterJS script available at:

 https://github.com/Zhouzi/TheaterJS.

 The script is defined as a third party library in theater.libraries.yml.


 INSTALLATION
 ------------

  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-8
    for further information.


MAINTAINERS
-----------

Current maintainers:
 * Carsten Høyer (c.hoyer) - https://www.drupal.org/user/2671607
