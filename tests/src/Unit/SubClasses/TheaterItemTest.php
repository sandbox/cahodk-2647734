<?php

/**
 * @file
 * Contains Drupal\Tests\theater\Unit\TheaterItemTest.
 */

namespace Drupal\Tests\theater\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\theater\Plugin\Field\FieldType\TheaterItem;
use Drupal\Core\TypedData\DataDefinitionInterface;

/**
 * FieldItem unit test.
 *
 * @ingroup theater
 *
 * @group theater
 * @group theater
 */
class TheaterItemTest extends UnitTestCase {

  protected $field;
  protected $context;
  protected $violation;

  /**
   * Setup before tests.
   */
  public function setUp() {
    $definition = $this->getMockBuilder('Drupal\Core\TypedData\DataDefinitionInterface')
      ->disableOriginalConstructor()
      ->setMethods(
        array(
          'createFromDataType',
          'getDataType',
          'getLabel',
          'getDescription',
          'isList',
          'isReadOnly',
          'isComputed',
          'isRequired',
          'getClass',
          'getSettings',
          'getSetting',
          'getConstraints',
          'getConstraint',
          'addConstraint',
          'getPropertyDefinitions',
        )
      )
      ->getMock();

    $definition->expects($this->any())
      ->method('getPropertyDefinitions')
      ->willReturn(array());

    $this->context = $this->getMockBuilder('Symfony\Component\Validator\Context\ExecutionContextInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $this->violation = $this->getMockBuilder('Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $this->field = new TheaterItem($definition);
  }

  /**
   * Handle Empty Input.
   */
  public function testItHandlesEmptyInput() {
    $str = '';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertFalse($success);
  }

  /**
   * Handle Input Without Semi-colon.
   */
  public function testItHandlesInputWithoutSemiColon() {
    $str = 'This string has no semicolon.';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertFalse($success);
  }

  /**
   * Handle input without colon.
   */
  public function testItHandlesInputWithoutColon() {
    $str = 'This string has no colon;';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertFalse($success);
  }

  /**
   * Handle actors with whitespace.
   */
  public function testItHandlesActorsWithWhitespace() {
    $str = 'Actor has space: atom;';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertFalse($success);
  }

  /**
   * Handle actors with special utf-8 chars.
   */
  public function testItHandlesActorsWithSpecialChars() {
    $str = 'Åctor: atom;';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertFalse($success);
  }

  /**
   * Handle actors with special brackets.
   */
  public function testItHandlesActorsWithBrackets() {
    $str1 = '<ctor>: atom;';
    $str2 = '(ctor): atom;';
    $str3 = '{ctor}: atom;';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str1, $this->context);
    $this->assertFalse($success);

    $success = $this->field->theaterValidate($str2, $this->context);
    $this->assertFalse($success);

    $success = $this->field->theaterValidate($str3, $this->context);
    $this->assertFalse($success);
  }

  /**
   * Handle script in atom.
   */
  public function testItHandlesScripting() {
    $str = 'actor: "string contains script <script></script>";';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertFalse($success);
  }

  /**
   * It accepts escaped commas.
   */
  public function testItAcceptsEscapedCommas() {
    $str = 'actor: "string contains\, escaped comma";';

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertNull($success);
  }

  /**
   * It accepts escaped semi-colons.
   */
  public function testItAcceptsEscapedSemiColons() {
    $str = 'actor: "string contains\; escaped semi-colon";';

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertNull($success);
  }

  /**
   * It accepts escaped colons.
   */
  public function testItAcceptsEscapedColons() {
    $str = 'actor: "string contains\: escaped colon";';

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertNull($success);
  }

  /**
   * It regards repeated strings in sentence as an error.
   */
  public function testItDoesNotAllowRepeatedStrings() {
    $str = 'actor: "string 1", "string 2";';

    $this->context->expects($this->any())
      ->method('buildViolation')
      ->willReturn($this->violation);

    $success = $this->field->theaterValidate($str, $this->context);
    $this->assertFalse($success);
  }

  /**
   * It validates a valid string.
   */
  public function testItValidatesValidString() {
    $str1 = 'actor: "string 1", 200, "string 2";';
    $str2 = 'actora: "string 1", 200, "string 2";';
    $str2 .= 'actorb: "string 1", 200, "string 2";';

    $success = $this->field->theaterValidate($str1, $this->context);
    $this->assertNull($success);

    $success = $this->field->theaterValidate($str2, $this->context);
    $this->assertNull($success);
  }

}
